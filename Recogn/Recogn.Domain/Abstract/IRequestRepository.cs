﻿using Recogn.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recogn.Domain.Abstract
{
    public interface IRequestRepository
    {
        List<Request> GetRequests();
        void AddRequest(Request word);
    }
}
