﻿using System.Collections.Generic;
using Recogn.Domain.Entities;

namespace Recogn.Domain.Abstract
{
    public interface IUserRepository
    {
        List<User> GetUsers();
        User GetUser(string login);
        void AddUser(User user);
        void UserUpdate(string login, int reqCount);
        void DateUpdate(string firstReq, string login);
    }
}