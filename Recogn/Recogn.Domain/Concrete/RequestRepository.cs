﻿using Recogn.Domain.Abstract;
using Recogn.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Recogn.Domain.Concrete
{
    public class RequestRepository : IRequestRepository
    {
        //private readonly string conStr = @"Data Source = D:\All\Projects\Git\TestProject\Recogn\Recogn.WebUI\App_Data\Logging.s3db";

        string conStr = ConfigurationManager.ConnectionStrings["Connection"].ConnectionString;

        private List<Request> RequestsList = new List<Request>();

        public List<Request> GetRequests()
        {
            using (SQLiteConnection con = new SQLiteConnection(conStr))
            {
                con.Open();
                string stm = "Select * from Requests";

                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            RequestsList.Add(new Request
                            {
                                ID = int.Parse(reader["ID"].ToString()),
                                Word = reader["Word"].ToString(),
                                English = reader["English"].ToString(),
                                Spanish = reader["Spanish"].ToString(),
                                Portuguese = reader["Portuguese"].ToString(),
                                Russian = reader["Russian"].ToString(),
                                Bulgarian = reader["Bulgarian"].ToString(),
                                Time = reader["Time"].ToString()
                            });
                        }
                        reader.Close();
                    }
                }
                con.Close();
            }
            return RequestsList;
        }

        public void AddRequest(Request request)
        {
            using (SQLiteConnection con = new SQLiteConnection(conStr))
            {
                con.Open();

                string stm = string.Format("Insert into Requests (Word, English, Spanish, Portuguese, Russian, Bulgarian, Time) values ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')",
                    request.Word.Replace("'", "''"), request.English, request.Spanish, request.Portuguese, request.Russian, request.Bulgarian, DateTime.Now);
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    cmd.ExecuteNonQuery();
                }

                con.Close();
            }
        }
    }
}
