﻿using System.Collections.Generic;
using Recogn.Domain.Entities;
using Recogn.Domain.Abstract;
using System.Data.SQLite;
using System;
using System.Configuration;

namespace Recogn.Domain.Concrete
{
    public class UserRepository : IUserRepository
    {
        //private readonly string conStr = @"Data Source = D:\All\Projects\Git\TestProject\Recogn\Recogn.WebUI\App_Data\Logging.s3db";

        string conStr = ConfigurationManager.ConnectionStrings["Connection"].ConnectionString;

        public IEnumerable<User> Users
        {
            get { return GetUsers(); }
        }

        private List<User> UsersList = new List<User>();
        private User User;

        public List<User> GetUsers()
        {
            using (SQLiteConnection con = new SQLiteConnection(conStr))
            {
                con.Open();
                string stm = "Select * from Users";

                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            UsersList.Add(new User
                            {
                                ID = int.Parse(reader["ID"].ToString()),
                                Login = reader["Login"].ToString(),
                                Email = reader["Email"].ToString(),
                                Password = reader["Password"].ToString(),
                                Company = reader["Company"].ToString(),
                                Role = reader["Role"].ToString(),
                                Date = reader["Date"].ToString(),
                                ReqCount = int.Parse(reader["ReqCount"].ToString()),
                                FirstReq = reader["FirstReq"].ToString(),
                                LastReq = reader["LastReq"].ToString()
                            });
                        }
                        reader.Close();
                    }
                }
                con.Close();
            }
            return UsersList;
        }

        public User GetUser(string login)
        {
            using (SQLiteConnection con = new SQLiteConnection(conStr))
            {
                con.Open();
                string stm = string.Format("Select * from Users where Login = '{0}'", login);

                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            User = new User
                            {
                                ID = int.Parse(reader["ID"].ToString()),
                                Login = reader["Login"].ToString(),
                                Email = reader["Email"].ToString(),
                                Password = reader["Password"].ToString(),
                                Company = reader["Company"].ToString(),
                                Role = reader["Role"].ToString(),
                                ReqCount = int.Parse(reader["ReqCount"].ToString())
                            };
                        }
                        reader.Close();
                    }
                }
                con.Close();
            }
            return User;
        }

        public void AddUser(User user)
        {
            using (SQLiteConnection con = new SQLiteConnection(conStr))
            {
                con.Open();

                string stm = string.Format("Insert into Users (Login, Email, Password, Company, Role, Date, ReqCount) values ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', {6})",
                    user.Login.Replace("'", "''"), user.Email.Replace("'", "''"), user.Password.Replace("'", "''"), user.Company.Replace("'", "''"), user.Role.Replace("'", "''"), DateTime.Now, user.ReqCount);
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    cmd.ExecuteNonQuery();
                }

                con.Close();
            }
        }

        public void UserUpdate(string login, int reqCount)
        {
            using (SQLiteConnection con = new SQLiteConnection(conStr))
            {
                con.Open();

                string stm = string.Format("Update Users set Date = '{0}', ReqCount = {1} where Login = '{2}'", DateTime.Now, reqCount, login);
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    cmd.ExecuteNonQuery();
                }

                con.Close();
            }
        }

        public void DateUpdate(string firstReq, string login)
        {
            using (SQLiteConnection con = new SQLiteConnection(conStr))
            {
                con.Open();

                string stm = string.Format("Update Users set FirstReq = '{0}', LastReq = '{1}' where Login = '{2}'", firstReq, DateTime.Now.ToString(), login);
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    cmd.ExecuteNonQuery();
                }

                con.Close();
            }
        }
    }
}