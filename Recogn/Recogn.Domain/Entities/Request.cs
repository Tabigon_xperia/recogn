﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recogn.Domain.Entities
{
    public class Request
    {
        public int ID { get; set; }
        public string Word { get; set; }
        public string English { get; set; }
        public string Spanish { get; set; }
        public string Portuguese { get; set; }
        public string Russian { get; set; }
        public string Bulgarian { get; set; }
        public string Time { get; set; }
    }
}
