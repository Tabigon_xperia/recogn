﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recogn.Domain.Entities
{
    public class User
    {
        public int ID { get; set; }

        [StringLength(50, MinimumLength = 3, ErrorMessage = "The field must be at least 3 and at max 50 characters long.")]
        [Required(ErrorMessage = "Please enter Login")]
        public string Login { get; set; }

        [StringLength(50, ErrorMessage = "The field must be at max 50 characters long.")]
        [RegularExpression(@"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$",
        ErrorMessage = "Please enter correct Email address")]
        public string Email { get; set; }

        [StringLength(50, ErrorMessage = "The field must be at max 50 characters long.")]
        public string Company { get; set; }

        [StringLength(50, MinimumLength = 3, ErrorMessage = "The field must be at least 3 and at max 50 characters long.")]
        [Required(ErrorMessage = "Please enter Password")]
        public string Password { get; set; }

        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string CPassword { get; set; }

        [StringLength(50, ErrorMessage = "The field must be at max 50 characters long.")]
        public string Role { get; set; }
        public string Date { get; set; }
        public int ReqCount { get; set; }
        public string FirstReq { get; set; }
        public string LastReq { get; set; }
    }
}
