﻿using System.Web.Http;

namespace Recogn.WebUI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "TwoParamRoute",
                routeTemplate: "api/{controller}/{num1}/{num2}"
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{num}",
                defaults: new { num = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "MainRoute",
                routeTemplate: "api/{controller}/{action}"
            );
        }
    }
}
