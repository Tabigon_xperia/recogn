﻿using Recogn.Domain.Abstract;
using Recogn.Domain.Concrete;
using Recogn.Domain.Entities;
using Recogn.WebUI.Extensions;
using Recogn.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;

namespace Recogn.WebUI.Controllers
{
    public class ActController : ApiController
    {
        private readonly IRequestRepository rRepository;
        private readonly IUserRepository uRepository;
        private List<Request> requests = new List<Request>();
        private List<User> users = new List<User>();

        public ActController()
        {
            this.rRepository = new RequestRepository();
            this.uRepository = new UserRepository();
            requests = rRepository.GetRequests();
            users = uRepository.GetUsers();
        }

        public ActController(IRequestRepository rRepository, IUserRepository uRepository)
        {
            this.rRepository = rRepository;
            this.uRepository = uRepository;
            requests = rRepository.GetRequests();
            users = uRepository.GetUsers();
        }

        public HttpResponseMessage Get()
        {
            CookieHeaderValue cookie = Request.Headers.GetCookies("Login").FirstOrDefault(); // аутентификация
            if (cookie == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "User unauthorized.");
            }

            List<UserViewModel> model = new List<UserViewModel>(); // модель для таблицы с информацией о пользователях

            int count = 0;
            foreach (var i in users.OrderByDescending(u => u.ReqCount))
            {
                count++;
                string str = "N/A"; // вычисление среднего времени между запросами (N/A, если меньше минуты)

                if ((i.FirstReq != null) && (i.FirstReq != ""))
                    if (((DateTime.Parse(i.LastReq) - DateTime.Parse(i.FirstReq)).TotalMinutes / i.ReqCount) >= 1)
                        str = ((DateTime.Parse(i.LastReq) - DateTime.Parse(i.FirstReq)).TotalMinutes / i.ReqCount).ToString("#.##");

                model.Add(new UserViewModel // заполнение модели
                {
                    Login = i.Login,
                    ReqCount = i.ReqCount,
                    Date = i.Date,
                    AverTime = str
                });
                if (count == 10)
                    break;
            }

            var response = Request.CreateResponse<List<UserViewModel>>(HttpStatusCode.OK, model);

            return response;
        }

        public HttpResponseMessage Post(string[] words)
        {
            CookieHeaderValue cookie = Request.Headers.GetCookies("Login").FirstOrDefault(); // аутентификация

            if (cookie == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "User unauthorized.");
            }
            User user = users.FirstOrDefault(u => u.Login == cookie["Login"].Value);

            Recognition Recogn = new Recognition(rRepository, uRepository);

            var response = Request.CreateResponse<List<Request>>(HttpStatusCode.OK, Recogn.Act(words, user));
            return response;
        }      
    }
}
