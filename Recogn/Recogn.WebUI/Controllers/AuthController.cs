﻿using Recogn.Domain.Abstract;
using Recogn.Domain.Concrete;
using Recogn.Domain.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;

namespace Recogn.WebUI.Controllers
{
    public class AuthController : ApiController
    {
        private readonly IUserRepository repository;

        public AuthController()
        {
            this.repository = new UserRepository();
        }

        public AuthController(IUserRepository repository)
        {
            this.repository = repository;
        }

        public HttpResponseMessage GetUser(string num1, string num2)
        {
            User user = repository.GetUser(num1);

            if (num1 == null)
                ModelState.AddModelError("user.Login", "Please enter Login");
            if (num2 == null)
                ModelState.AddModelError("user.Password", "Please enter Password");

            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState); 
            else
            {
                if (user == null)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "User not found");
                else if (user.Password != num2)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Password is not valid");
            }

            repository.UserUpdate(num1, user.ReqCount);

            var resp = new HttpResponseMessage();

            var cookie = new CookieHeaderValue("Login", num1);
            cookie.Expires = DateTimeOffset.Now.AddDays(1);
            cookie.Domain = Request.RequestUri.Host;
            cookie.Path = "/";
            var response = Request.CreateResponse(HttpStatusCode.OK);
            resp.Headers.AddCookies(new CookieHeaderValue[] { cookie });

            return resp;
        }

        [HttpPost]
        public HttpResponseMessage CreateUser([FromBody]User user)
        {
            if (repository.GetUsers().FirstOrDefault(n => n.Login == user.Login) != null)
                ModelState.AddModelError("user.Login", "The same Login already exists. Reload the page to enter with this Login,");
            if ((user.Email != "") && (repository.GetUsers().FirstOrDefault(n => n.Email == user.Email) != null))
                ModelState.AddModelError("user.Email", "The same Email already exists.");
            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            repository.AddUser(user);

            var resp = new HttpResponseMessage();

            var cookie = new CookieHeaderValue("Login", user.Login);
            cookie.Expires = DateTimeOffset.Now.AddDays(1);
            cookie.Domain = Request.RequestUri.Host;
            cookie.Path = "/";
            var response = Request.CreateResponse(HttpStatusCode.OK);
            resp.Headers.AddCookies(new CookieHeaderValue[] { cookie });

            return resp;
        }
    }
}
