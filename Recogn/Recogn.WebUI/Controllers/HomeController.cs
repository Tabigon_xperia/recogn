﻿using Recogn.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Recogn.WebUI.Controllers
{
    public class HomeController : Controller
    {
        private IUserRepository repository;

        public HomeController(IUserRepository repo)
        {
            repository = repo;
        }

        public ActionResult Index()
        {
            return View(repository.GetUsers());
        }
    }
}