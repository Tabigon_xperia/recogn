﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recogn.WebUI.Extensions
{
    public class Range
    {
        public Range(int minValue, int maxValue)
        {
            MinValue = minValue;
            MaxValue = maxValue;
        }

        public int MinValue { get; set; } 

        public int MaxValue { get; set; }
    }

    public class Start
    {
        public static int GetRandomFromRange(Random rnd, params Range[] ranges)
        {
            int idx = rnd.Next(ranges.Length);
            Range range = ranges[idx];
            return rnd.Next(range.MinValue, range.MaxValue);
        }
    }
}
