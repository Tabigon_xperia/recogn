﻿using Recogn.Domain.Abstract;
using Recogn.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Recogn.WebUI.Extensions
{
    public class Recognition
    {
        private readonly IRequestRepository rRepository;
        private readonly IUserRepository uRepository;
        private List<Request> requests = new List<Request>();
        private List<User> users = new List<User>();

        private readonly List<char[]> alphabet = new List<char[]>()
        {
            new char[] {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'},
            new char[] {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ñ'},
            new char[] {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'x', 'z', 'á', 'â', 'ã', 'à', 'ç', 'é', 'ê', 'í', 'ó', 'ô', 'õ', 'ú'},
            new char[] {'б', 'в', 'г', 'д', 'ж', 'з', 'й', 'к', 'л', 'м', 'н', 'п', 'р', 'с', 'т', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'о', 'у', 'а', 'и', 'е', 'ё', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'},
            new char[] {'б', 'в', 'г', 'д', 'ж', 'з', 'й', 'к', 'л', 'м', 'н', 'п', 'р', 'с', 'т', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'о', 'у', 'а', 'и', 'е', 'ъ', 'ь', 'ю', 'я'}
        };

        private readonly List<char[]> dblLetters = new List<char[]>()
        {
            new char[] {'l', 's', 'e', 'o', 't', 'f', 'p', 'r', 'm', 'c', 'n', 'd', 'g', 'i', 'b', 'a', 'z', 'x', 'u', 'h'},
            new char[] {'c', 'l', 'r'},
            new char[] {'s', 'r', 'n', 'm'},
            new char[] {'б', 'г', 'д', 'е', 'ж', 'з', 'и', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'ф', 'ш', 'ю', 'я'},
            new char[] {'б', 'г', 'д', 'е', 'ж', 'з', 'и', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'ф', 'ш', 'ю', 'я'}
        };

        private readonly string[] enRul = new string[] { "th", "ir" };
        private readonly char[] commonPunct = new char[] { '\'', '!', '#', '$', '%', '?', ')', '-', '—', '=', '+', ':', ';', '"', ',', '.' };
        private string[] results = new string[5];

        public Recognition(IRequestRepository rRepository, IUserRepository uRepository)
        {
            this.rRepository = rRepository;
            this.uRepository = uRepository;
            requests = rRepository.GetRequests();
            users = uRepository.GetUsers();
        }

        public List<Request> Act(string[] words, User user)
        {
            List<Request> req = new List<Request>();
            Request r = new Request();

            int kolvo = 0;

            foreach (var i in words)
            {
                if ((i.Trim() != "") && (i != null)) // если не пустое слово
                {
                    string word = i.Trim();
                    if (i.IndexOf("\n") >= 0) // отсекаем возможные символы переходов
                        word = word.Substring(0, i.IndexOf("\n"));
                    if (Array.IndexOf(commonPunct, word[word.Length - 1]) > 0) // если последний символ - разрешённый символ пунктуации, отсекаем
                        word = word.Substring(0, word.Length - 1);

                    if (word.Length < 4) // если размер слова после отсечения ненужных символов меньше 4х
                        req.Add(new Request
                        {
                            Word = word,
                            English = null,
                            Spanish = null,
                            Portuguese = null,
                            Russian = null,
                            Bulgarian = null
                        });
                    else
                    {
                        kolvo++;

                        if ((user.FirstReq == null) || (user.FirstReq == "")) // если это был первый запрос, устанавливаем дату первого и последнего запроса, иначе - только последнего
                            uRepository.DateUpdate(DateTime.Now.ToString(), user.Login);
                        else
                            uRepository.DateUpdate(user.FirstReq.ToString(), user.Login);

                        r = requests.FirstOrDefault(n => n.Word.ToLower() == word.ToLower()); // ищем слово в уже проходивших обработку
                        if (r == null) // не нашли
                        {
                            int lang = 0, dbl = 0;
                            foreach (var alph in alphabet) // сравниваем слово с каждым из пяти алфавитов
                            {
                                int schet = 0; // количество найденых ошибок

                                for (int ch = 0; ch < word.Length; ch++)
                                {
                                    if (Array.IndexOf(alph, word.ToLower()[ch]) < 0) // для начала сравниваем каждый символ с допустимыми символами для данного языка
                                        schet++;
                                    if (((word.ToLower()[ch] == '\'') && (lang == 0)) || ((word.ToLower()[ch] == '-') && (lang == 3)))
                                        schet--; // уменьшаем счётчик, если для английского языка обнаружилась кавычка или для русского - дефис
                                    if (ch > 0)
                                    {
                                        if (word.ToLower()[ch] == word.ToLower()[ch - 1]) // проверяем на множественные буквы
                                            dbl++;
                                        else
                                        {
                                            schet += dbl;
                                            if ((Array.IndexOf(dblLetters[lang], word.ToLower()[ch - 1]) >= 0) && (dbl > 0))
                                                schet--;
                                            dbl = 0;
                                        }
                                    }
                                    if ((lang == 3) && (word.ToLower()[ch] == 'ъ') && (ch > 0) && (ch != word.Length - 1)) // проверка на ъ и ь в русском и болгарском языках
                                    {
                                        if ((Array.IndexOf(alph, word.ToLower()[ch - 1]) >= 0) && (Array.IndexOf(alph, word.ToLower()[ch - 1]) <= 20)
                                         && (Array.IndexOf(alph, word.ToLower()[ch + 1]) >= 0) && (Array.IndexOf(alph, word.ToLower()[ch + 1]) <= 20))
                                            schet++;
                                    }
                                    if (((word.ToLower()[ch] == 'ъ') || (word.ToLower()[ch] == 'ь')) && (ch > 0))
                                        if (Array.IndexOf(alph, word.ToLower()[ch - 1]) >= 21)
                                            schet++;
                                }
                                if ((lang == 1) && ((word[0] == '¿') || (word[0] == '¡'))) // проверка на особые знаки в испанском языке
                                    schet--;
                                if ((lang == 3) && ((word[0] == '"') || (word[0] == '('))) // проверка на кавычки в русском языке
                                    schet--;

                                if ((lang == 3) || (lang == 4)) // проверка на ъ или ь первыми буквами или последними в русском
                                {
                                    if ((word.ToLower()[0] == 'ь') || (word.ToLower()[0] == 'ъ') || (word.ToLower()[0] == 'ы'))
                                        schet++;
                                    if ((lang == 3) && (word.ToLower()[word.Length - 1] == 'ъ'))
                                        schet++;
                                }

                                foreach (var n in enRul) // доп. правила в английском
                                {
                                    if ((lang != 0) && (word.ToLower().IndexOf(n) >= 0))
                                    {
                                        string curr = word.ToLower();
                                        while (curr.IndexOf(n) >= 0)
                                        {
                                            schet++;
                                            curr = curr.Substring(0, curr.IndexOf(n)) + curr.Substring(curr.IndexOf(n) + 2);
                                        }
                                    }
                                }

                                if (schet >= word.Length) // расчёт процентов
                                    results[lang] = "";
                                else
                                    results[lang] = ((word.Length - schet) / ((decimal)word.Length / 100)).ToString("#.##");
                                lang++;
                            }

                            r = new Request // добавление слова в БД
                            {
                                Word = word,
                                English = results[0],
                                Spanish = results[1],
                                Portuguese = results[2],
                                Russian = results[3],
                                Bulgarian = results[4]
                            };
                            rRepository.AddRequest(r);
                        }

                        req.Add(new Request // если нашли слово в БД
                        {
                            Word = i,
                            English = r.English,
                            Spanish = r.Spanish,
                            Portuguese = r.Portuguese,
                            Russian = r.Russian,
                            Bulgarian = r.Bulgarian
                        });
                    }
                }
            }
            uRepository.UserUpdate(user.Login, user.ReqCount + kolvo); // увеличиваем кол-во запросов пользователя к словарю
            return req;
        }
    }
}