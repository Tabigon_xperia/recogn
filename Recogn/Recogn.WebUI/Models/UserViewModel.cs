﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Recogn.WebUI.Models
{
    public class UserViewModel
    {
        public string Login { get; set; }
        public int ReqCount { get; set; }
        public string Date { get; set; }
        public string AverTime { get; set; }
    }
}